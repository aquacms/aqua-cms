aqua-cms
============

Early version of the aqua-cms, unfinished and still has a lot of work to be done.

Uses components from the Symfony framework & Schuurman framework.
Inspired by many different Frameworks / CMSs.

Main goal: Be a solid foundation for any web application that needs to be developed

Team
============

* Developers
  * Jorin Vermeulen - https://www.jorinvermeulen.com/
  
Credits
============
* Logo Design
  * Nick Nedashkovskii - https://dribbble.com/Dessgner
* HtAccess Improvements
  * Creare - https://github.com/Creare/magento-htaccess