<?php

/**
 *  The aqua-cms is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The aqua-cms is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use Aqua_DataType_Model_String as String;

/**
 * Description of Validate
 *
 * @TODO complete rewrite and expansion
 */
final class Aqua_Security_Model_Validate {
	/**
	 * Validate a string whether it's a valid URL or not, returns TRUE if it's valid
	 * @param string $input
	 * @return boolean
	 */
	public static function isUrl($input) {
		return filter_var(String::cast($input), FILTER_VALIDATE_URL) === true;
	}
	
	/**
	 * Validate a string whether it's a valid Route URL or not, returns TRUE if it's valid
	 * @param string $input
	 * @return boolean
	 */
	public static function isRouteUrl($input) {
		return preg_match('/^[a-zA-Z0-9_-:]+$/', String::cast($input)) === 1;
	}
	
	/**
	 * Makes sure a string is NOT empty, returns FALSE if it's empty.
	 * set the strict parameter to FALSE if you don't want (for example) "   " pass as not-empty
	 * @param string $input
	 * @param boolean $strict
	 * @return boolean
	 */
	public static function isNotEmpty($input, bool $strict = true) {
        $input = String::cast($input);

		if($strict === true) {
			return $input !== "" && $input !== null;
		} else {
			return empty($input);
		}
	}
}