<?php


use Aqua_DataType_Model_String as String;
use Aqua_DataType_Model_Boolean as Boolean;

/**
 * Registry class containing a register of keys with their respective values
 *
 * @package AquaCMS\Code\Core\Aqua\Registry\Model
 */
class Aqua_Registry_Model_Registry {
    /**
     * Holds all the keys and their values
     *
     * @var array|null
     */
    protected $_register = null;

    /**
     * Create instance of the Registry
     */
    public function __construct() {
        $this->_register = array();
    }

    /**
     * Get a value from the registry by key
     *
     * @param $key
     * @param bool $graceful
     * @throws Exception
     */
    public function get($key, bool $graceful = null) {
        $key        = String::cast($key);
        $graceful   = Boolean::cast($graceful);

        if(isset($this->_register[$key]) === false) {
            if($graceful === false) {
                throw new Exception("This key does not exist in the registry");
            }

            return null;
        }

        return $this->_register[$key];
    }

    /**
     * Set a value from the registry by key
     * if $update is set to true it will update any existing key
     *
     * @param $key
     * @param $value
     * @param bool $update
     * @param bool $graceful
     * @return bool
     * @throws Exception
     */
    public function set($key, $value, bool $update = null, bool $graceful = null) {
        $key        = String::cast($key);
        $update     = Boolean::cast($update);
        $graceful   = Boolean::cast($graceful);

        if(isset($this->_register[$key]) && $update === false) {
            //If the key already exists and it isn't set to update when it exists, throw exception
            if($graceful === false) {
                throw new Exception("This key already exists in the registry");
            }

            //Return false when it fails to update
            return false;
        } else {
            $this->_register[$key] = String::cast($value);
        }

        //Return true when succeeded to set or update the key into the registry
        return true;
    }

    /**
     * Delete a key from the registry
     *
     * @param $key
     * @return bool
     */
    public function delete($key) {
        $key = String::cast($key);

        try {
            //Get the item from the registry
            $item = $this->get($key);

            //Check if the item is an object or instance, if it is and it has the __destruct method, call it
            if(is_object($item) === true  && method_exists($item, '__destruct') === true) {
                $item->__destruct();
            }

            //Delete the item from the register
            unset($this->_register[$key]);
        } catch (Exception $exception) {
            //Failed to remove, return false
            return false;
        }

        //Removed, return true
        return true;
    }
}