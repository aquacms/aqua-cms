<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

abstract class Aqua_Block_Model_Abstract {
    
    protected $id;
    protected $children;
    
    abstract protected function _toHtml();
    
    public function getId() {
        return $this->id;
    }
    
    public function getChildren() {
        return $this->children;
    }
    
    /**
     * Set the ID of this block
     * @param int $id
     * @return Aqua_Block_Model_Block
     */
    public function setId(int $id) {
        $this->id = $id;
        return $this;
    }
    
}