<?php
/**
 * The DataType for an array.
 * @author Arjan Schuurman
 */

class Aqua_Core_Model_DataType_Array implements Aqua_Core_Model_DataType_IDataType {

    const DEFAULT_VALUE = null;

	/**
	 * Casts the given value to an array.
	 * @param mixed $value The value to be casted.
	 * @return array The casted value.
	 */
	public static function cast($value) {
		return (array)$value;
	}

	/**
	 * Checks whether the given value is an array.
	 * @param mixed $value The value to be checked.
	 * @return bool Whether the given value is an array.
	 */
	public static function isType($value) {
		return (is_array($value) === true);
	}

    /**
     * Checks whether the given value is an empty array
     * @param array $input
     * @return bool
     */
    public static function isDefault($input) {
        return $input === array();
    }
}