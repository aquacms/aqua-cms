<?php
/**
 * The DataType for a string.
 * @author Arjan Schuurman
 */

class Aqua_Core_Model_DataType_String implements Aqua_Core_Model_DataType_IDataType {
	/**
	 * The default value of a string.
	 * @var string
	 */
	const DEFAULT_VALUE = '';

	/**
	 * Casts the given value to string.
	 * @param mixed $value The value to be casted.
	 * @return string The casted value.
	 */
	public static function cast($value) {
		return (string) $value;
	}

	/**
	 * Checks if the specified string is equal to the default value.
	 * @param string $value The string to be checked.
	 * @return bool Whether the given string is equal to the default value.
	 */
	public static function isDefault($value) {
		if ($value === self::DEFAULT_VALUE) {
			return true;
		}

		return false;
	}

	/**
	 * Checks whether the given value is a string.
	 * @param mixed $value The value to be checked.
	 * @return bool Whether the given value is a string.
	 */
	public static function isType($value) {
		return (is_string($value) === true);
	}
}