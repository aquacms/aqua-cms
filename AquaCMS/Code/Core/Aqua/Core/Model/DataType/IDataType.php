<?php

interface Aqua_Core_Model_DataType_IDataType {

    public static function cast($input);
    public static function isDefault($input);
    public static function isType($input);
}