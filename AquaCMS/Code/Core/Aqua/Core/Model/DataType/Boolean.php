<?php
/**
 * The DataType for a boolean.
 * @author Arjan Schuurman
 */

class Aqua_Core_Model_DataType_Boolean implements Aqua_Core_Model_DataType_IDataType {

	/**
	 * The default value of a bool.
	 * @var bool
	 */
    const DEFAULT_VALUE = false;

	/**
	 * Casts the given value to a bool.
	 * @param mixed $value The value to be casted.
	 * @return bool The casted value.
	 */
	public static function cast($value) {
		if (strtoupper($value) === "N" || is_null($value)) {
			return false;
		}

		return (bool)$value;
	}

	/**
	 * Checks whether the given value is a boolean.
	 * @param mixed $value The value to be checked.
	 * @return bool Whether the given value is a boolean.
	 */
	public static function isType($value) {
		return (is_bool($value) === true);
	}

    public static function isDefault($input)
    {
        return $input === self::DEFAULT_VALUE;
    }
}