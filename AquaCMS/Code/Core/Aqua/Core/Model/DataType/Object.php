<?php
/**
 * The DataType for an object.
 * @author Arjan Schuurman
 */

class Aqua_Core_Model_DataType_Object implements Aqua_Core_Model_DataType_IDataType {
	/**
	 * The default value of an object.
	 * @var null
	 */
    const DEFAULT_VALUE = null;

	/**
	 * Checks if the specified object is equal to the default value.
	 * @param object $value The object to be checked.
	 * @return bool Whether the given object is equal to the default value.
	 */
	public static function isDefault($value) {
		if ($value === self::DEFAULT_VALUE) {
			return true;
		}

		return false;
	}

	/**
	 * Checks whether the given value is an object.
	 * @param mixed $value The value to be checked.
	 * @return bool Whether the given value is an object.
	 */
	public static function isType($value) {
		return (is_object($value) === true);
	}

	/**
	 * Checks whether the give property exists in the given object.
	 * @param stdClass $object The object to check the property existence for.
	 * @param string $property The property to check the existence for.
	 * @return Whether the property exists in the object.
	 */
	public static function propertyExists($object, $property) {
		return (property_exists($object, $property) === true);
	}

    /**
     * Casts the input value to an object
     * @param mixed $input
     * @return object
     */
    public static function cast($input) {
        return (object) $input;
    }
}