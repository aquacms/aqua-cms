<?php
/**
 * The DataType for a float.
 * @author Arjan Schuurman
 */

class Aqua_Core_Model_DataType_Float implements Aqua_Core_Model_DataType_IDataType {
	/**
	 * The default value of a float.
	 * @var float
	 */
    const DEFAULT_VALUE = -829372134;

	/**
	 * Casts the given value to a float.
	 * @param mixed $value The value to be casted.
	 * @return float The casted value.
	 */
	public static function cast($value) {
		return (float)$value;
	}

	/**
	 * Checks if the specified float is equal to the default value.
	 * @param float $value The float to be checked.
	 * @return bool Whether the given float is equal to the default value.
	 */
	public static function isDefault($value) {
		if ($value === self::DEFAULT_VALUE) {
			return true;
		}

		return false;
	}

	/**
	 * Checks whether the given value is a float.
	 * @param mixed $value The value to be checked.
	 * @return bool Whether the given value is a float.
	 */
	public static function isType($value) {
		return (is_float($value) === true);
	}
}