<?php
/**
 * The DataType for an integer.
 * @author Arjan Schuurman
 */

class Aqua_Core_Model_DataType_Integer implements Aqua_Core_Model_DataType_IDataType {
	/**
	 * The default value of an integer.
	 * @var int
	 */
    const DEFAULT_VALUE = -829374221;

	/**
	 * Casts the given value to an integer.
	 * @param mixed $value The value to be casted.
	 * @return int The casted value.
	 */
	public static function cast($value) {
		return (int)$value;
	}

	/**
	 * Checks if the specified integer is equal to the default value.
	 * @param int $value The integer to be checked.
	 * @return bool Whether the given integer is equal to the default value.
	 */
	public static function isDefault($value) {
		if ($value === self::DEFAULT_VALUE) {
			return true;
		}

		return false;
	}

	/**
	 * Checks whether the given value is an integer.
	 * @param mixed $value The value to be checked.
	 * @return bool Whether the given value is an integer.
	 */
	public static function isType($value) {
		return (is_int($value) === true);
	}
}