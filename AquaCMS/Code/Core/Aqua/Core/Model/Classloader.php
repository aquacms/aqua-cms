<?php

class Aqua_Core_Model_Classloader {
    /**
     * A registry of instances to be used for singletons
     * @var array 
     */
    private static $_instance = null;
    
    /**
     * The instance of the classloader itself
     * @var Classloader
     */
    private $_classloaderInstance = null;
    
    private function __construct() {
        $this->_instance = array();
    }

    /**
     * instead of using include() use this function.
     * It checks whether or not a copy of the file is in the Extension or Local directory that takes priority
     *
     * This function only works for files in de AquaCMS\Code directory
     *
     * @param $file
     */
    public static function getFile($file) {
        if(file_exists(APP_DIR . 'Code' . DS . 'Local' . DS . $file)) {
            $file = APP_DIR . 'Code' . DS . 'Local' . DS . $file;
        } elseif(file_exists(APP_DIR . 'Code' . DS . 'Extension' . DS . $file)) {
            $file = APP_DIR . 'Code' . DS . 'Extension' . DS . $file;
        } else {
            $file = APP_DIR . 'Code' . DS . 'Core' . DS . $file;
        }

        return $file;
    }

    /**
     * Autoload function to use with spl_autoload_register()
     *
     * @param type $class_name
     * @return boolean
     */
    public static function autoload($class_name) {

        $class = explode('\\', $class_name);

        $class_name = str_replace((DS === '/') ? '\\' : '/',
            (DS === '/') ? '/' : '\\',
            $class_name);

        $filename = $class_name . '.php';
        $file = BASE_DIR . $filename;

        if (!file_exists($file) || !is_readable($file)) {
            return false;
        }

        include($file);

        return true;
    }
    
    /**
     * Get a singleton instance from the classloader
     * @return Classloader
     */
    public static function load() {
        if(self::$_classloaderInstance == null) {
            self::$_classloaderInstance = new self();
        }
        
        return self::$_classloaderInstance;
    }
    
    /**
     * Retrieve model object
     *
     * @TODO implement XML (module/extension) classloading to register classes for the autoloading
     *
     * Author_Module_<Model|Controller|Etc>_Classname
     *
     * <Config>
     *      <package>
     *          <Aqua_Classloader>
     *              <author name="Jorin Vermeulen" url="https://www.jorinvermeulen.com/" />
     *              <models>
     *                  <class>Aqua_Classloader_Model_Classloader</class>
     *              </models>
     *          </Aqua_Classloader>
     *          <Aqua_DataType>
     *              <models>
     *                  <class>Aqua_DataType_Model_Array</class>
     *                  <class>Aqua_DataType_Model_Boolean</class>
     *                  <class>Aqua_DataType_Model_Object</class>
     *                  <class>Aqua_DataType_Model_String</class>
     *              </models>
     *          </Aqua_DataType>
     *      </package>
     * </Config>
     *
     * @return  Aqua_MVC_Model_Abstract|false
     */
    public static function getModel($className, array $options = array()) {
        if(file_exists(APP_DIR . DS . 'local' . DS . $className . '.php')) {
            
        }

        return new $className();
    }

    /**
     * Retrieve model object singleton
     * 
     * @param   string $className
     * @param   array $options
     * @return  Aqua_MVC_Model_Abstract|false
     */
    public static function getSingleton($className, array $options = array()) {
        $instanceName = 'singleton_' . $className;
        
        if (!isset(self::$_instance[$instanceName])) {
            self::$_instance[$instanceName] = self::getModel($className, $options);
        }
        
        return self::$_instance[$instanceName];
    }
}