<?php

use Aqua_Core_Model_DataType_String as String;

class Aqua_Core_Model_PackageManager_Author {

    /**
     * Author name
     * @var
     */
    private $_name;

    /**
     * Author website
     * @var
     */
    private $_website;

    public function __construct($name, $website) {
        $this->_name        = String::cast($name);
        $this->_website     = String::cast($website);
    }

    /**
     * Get the name of the author
     * @return string
     */
    public function getName() {
        return $this->_name;
    }

    /**
     * Get the website of the author
     * @TODO validate url
     * @return string
     */
    public function getWebsite() {
        return $this->_website;
    }
}