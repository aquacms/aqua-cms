<?php

use Aqua_Core_Model_DataType_String as String;
use Aqua_Core_Model_PackageManager_Author as Author;

class Aqua_Core_Model_PackageManager_Package {

    /**
     * The name of the package
     * @var
     */
    protected $_name = null;

    /**
     * The version of the package
     * @var int
     */
    protected $_version = -1;

    /**
     * The website of the package
     * @var
     */
    protected $_website = null;

    /**
     * The author of the package
     * @var
     */
    protected $_author = null;

    /**
     * Constructor for creating a package object instance
     * @TODO Custom Aqua Exception
     * @param $name
     * @param $version
     * @param string $website
     * @param Aqua_Core_Model_PackageManager_Author $author
     * @throws Exception
     */
    public function __construct($name, $version, $website = '', Author $author = null) {
        //Make sure the author is either null or an instance of Author
        if((is_null($author) || $author instanceof Author) === false) {
            throw new Exception("Invalid Author provided");
        }

        //Cast (where needed) and Set the values
        $this->_name        = String::cast($name);
        $this->_version     = String::cast($version);
        $this->_website     = String::cast($website);
        $this->_author      = $author;
    }

    /**
     * Get the name of the package
     * @return mixed
     */
    public function getName() {
        return $this->_name;
    }

    /**
     * Get the website of the package
     * @return mixed
     */
    public function getWebsite() {
        return $this->_website;
    }

    /**
     * Get the author of the package
     * @return mixed
     */
    public function getAuthor() {
        return $this->_author;
    }
}