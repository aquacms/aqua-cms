<?php

/**
 *  The aqua-cms is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The aqua-cms is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use Aqua_Core_Model_FileSystem as FileSystem;
use Aqua_Core_Model_FileSystem_File as File;
use Aqua_Core_Model_PackageManager_Package as Package;

class Aqua_Core_Model_PackageManager {

    /**
     * The installed packages
     * @var
     */
    protected $_installedPackages = null;

    public function __construct() {
        $this->_installedPackages = $this->getInstalledPackageList();
    }

    /**
     * Creates a list of the installed packages
     * @TODO Exception handling
     * @return array
     */
    public function getInstalledPackageList() {
        //Get the contents of the AquaCMS/Etc/Package directory
        $packageFileList = FileSystem::get(APP_DIR . "Etc" . DS . "Packages")->getContents();

        $result = array();

        //Parse each object found in the directory
        foreach($packageFileList as $pkgConfig) {
            //Only parse xml files
            if($pkgConfig instanceof File === false) {
                continue;
            }

            //Get the full path of the xml file and parse it
            $result[] = $this->parsePackageXML($pkgConfig->getFullPath());
        }

        //Temp, debugging/dev purposes
        echo '<pre>';
        var_dump($result);

        return $result;
    }

    /**
     * Parses an Package XML file and returns the resulting Package Object
     * @TODO Custom Exception
     * @param $xmlFile
     * @throws Exception
     * @return Package
     */
    protected function parsePackageXML($xmlFile) {
        $xmlFile = realpath($xmlFile);
        return $xmlFile;
    }
}