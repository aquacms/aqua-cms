<?php

/**
 *  The aqua-cms is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The aqua-cms is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use Aqua_Core_Model_FileSystem_Directory as Directory;
use Aqua_Core_Model_FileSystem_File as File;

class Aqua_Core_Model_FileSystem {

    public function __construct() {

    }

    /**
     * Get an filesystem object from the specified path
     *
     * @TODO add option to exclude special directories like "." and ".." from showing up
     *
     * @param String $path
     * @return Aqua_Core_Model_FileSystem_IFileSystemObject
     */
    public static function get($path) {
        $lookupPath = realpath($path);

        //Directory
        if(is_dir($lookupPath)) {
            return new Directory($path, is_link($path));
        }
        //File
        elseif(is_file($lookupPath)) {
            return new File($path, is_link($path));
        }

        return null;
    }

    /**
     * Create a directory on file depending on $path
     * @param string $path
     * @return Aqua_Core_Model_FileSystem_IFileSystemObject
     */
    public static function create($path) {
        return null;
    }
    
    /**
     * Copy a filesystem object
     * @param Aqua_Core_Model_FileSystem_IFileSystemObject $obj
     * @param string $location
     * @return Aqua_Core_Model_FileSystem_IFileSystemObject
     */
    public static function copy(Aqua_Core_Model_FileSystem_IFileSystemObject $obj, $location) {
        return null;
    }
    
    /**
     * Move a filesystem object
     * @param Aqua_Core_Model_FileSystem_IFileSystemObject $obj
     * @param string $location
     * @return Aqua_Core_Model_FileSystem_IFileSystemObject
     */
    public static function move(Aqua_Core_Model_FileSystem_IFileSystemObject $obj, $location) {
        return null;
    }
    
    /**
     * Delete a filesystem object
     * @param Aqua_Core_Model_FileSystem_IFileSystemObject $obj
     * @return bool
     */
    public static function delete(Aqua_Core_Model_FileSystem_IFileSystemObject $obj) {
        return false;
    }
    
    /**
     * 
     * @param Aqua_Core_Model_FileSystem_File $file
     * @param string $content
     * @param boolean $append
     * @return boolean
     */
    public static function write(Aqua_Core_Model_FileSystem_File $file, $content, $append) {
        return false;
    }
}