<?php

/**
 *  The aqua-cms is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The aqua-cms is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

interface Aqua_Core_Model_FileSystem_IFileSystemObject {

    /**
     * Creates a filesystem object
     * @param $path
     * @param bool $symlink indicates whether this object is a symlink
     */
    public function __construct($path, $symlink);

    /**
     * Get the name of the filesystem object
     * @return string
     */
    public function getName();

    /**
     * Get the path of the directory the filesystem object remains in
     * @return string
     */
    public function getPath();

    /**
     * Get the full path of the filesystem object (including the name of the filesystem object itself)
     * @return string
     */
    public function getFullPath();

    /**
     * Get the size of a filesystem object
     * @return int
     */
    public function getSize();
}