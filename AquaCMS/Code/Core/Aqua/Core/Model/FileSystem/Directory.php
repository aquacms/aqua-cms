<?php

/**
 *  The aqua-cms is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The aqua-cms is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use Aqua_Core_Model_FileSystem as FileSystem;
use Aqua_Core_Model_DataType_Boolean as Boolean;

class Aqua_Core_Model_FileSystem_Directory implements Aqua_Core_Model_FileSystem_IFileSystemObject {
    /**
     * The name of the directory
     * @var string
     */
    protected $_name;

    /**
     * Our pointer of the current directory
     * @var string
     */
    protected $_currentWorkingDir;

    /**
     * Whether this is a symlink to another directory
     * @var bool
     */
    protected $_isSymlink;

    /**
     * Directory object constructor
     * @param $path
     * @param bool $isSymlink is this originally a symlink
     */
    public function __construct($path, $isSymlink) {
        $dirInfo = pathinfo($path);

        $this->_name                = $dirInfo['filename'];
        $this->_currentWorkingDir   = $dirInfo['dirname'] . DS;
        $this->_isSymlink           = Boolean::cast($isSymlink);
    }

    /**
     * Get the name of the filesystem object
     * @return string
     */
    public function getName() {
        return $this->_name;
    }

    /**
     * Get the path of the directory the filesystem object remains in
     * @return string
     */
    public function getPath() {
        return $this->_currentWorkingDir;
    }

    /**
     * Get the full path of the filesystem object (including the name of the filesystem object itself)
     * @return string
     */
    public function getFullPath() {
        return $this->getPath() . $this->getName() . DS;
    }

    /**
     * Get the size of the directory
     * @return int
     */
    public function getSize() {

    }

    /**
     * Return the content of the directory, such as files, symlinks and directories.
     * @TODO symlink loop detection/protection
     * @TODO what if there are thousands of files in a directory? limit amount shown? return iterator via result obj?
     * @return array
     */
    public function getContents() {
        $contents = scandir($this->getFullPath());

        $result = array();

        foreach($contents as $item) {
            $result[] = FileSystem::get($this->getFullPath() . $item);
        }

        return $result;
    }
}