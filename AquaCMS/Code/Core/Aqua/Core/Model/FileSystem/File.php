<?php

use Aqua_Core_Model_DataType_Boolean as Boolean;

class Aqua_Core_Model_FileSystem_File implements Aqua_Core_Model_FileSystem_IFileSystemObject {

    /**
     * The name of the file
     * @var string
     */
    protected $_name;

    /**
     * The file-extension
     * @var string
     */
    protected $_extension;

    /**
     * Our pointer of the current directory
     * @var string
     */
    protected $_currentWorkingDir;

    /**
     * Whether this is a symlink to another directory
     * @var bool
     */
    protected $_isSymlink;

    /**
     * @param $path
     * @param bool $symlink
     */
    public function __construct($path, $isSymlink) {
        $fileInfo = pathinfo($path);

        $this->_name                = $fileInfo['filename'];
        $this->_extension           = $fileInfo['extension'];
        $this->_currentWorkingDir   = $fileInfo['dirname'] . DS;
        $this->_isSymlink           = Boolean::cast($isSymlink);
    }

    /**
     * Get the name of the filesystem object (Without extension)
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Get the filename of the filesystem object
     * @return string
     */
    public function getFilename() {
        return $this->getName() . '.' . $this->getExtension();
    }

    /**
     * Get the extension from the file
     */
    public function getExtension() {
        return $this->_extension;
    }

    /**
     * Get the path of the directory the filesystem object remains in
     * @return string
     */
    public function getPath()
    {
        return $this->_currentWorkingDir;
    }

    /**
     * Get the full path of the filesystem object (including the name of the filesystem object itself)
     * @return string
     */
    public function getFullPath()
    {
        return $this->getPath() . $this->getFilename();
    }

    public function getSize() {

    }
}