<?php

/**
 *  The aqua-cms is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The aqua-cms is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use Aqua_DataType_Model_String as String;

/**
 * Parse URL's and match it against the available Routes, depending on the Route a controller will be called
 *
 * @author Jorin Vermeulen
 */
class Aqua_Router_Model_Router {
	
	/**
	 * Contains all Routes
	 * @var array
	 */
	private $map;
	
	public function __construct() {}
	
	/**
	 * Add a Route to the Map
	 * @param \AquaCMS\Code\Core\Aqua\Router\Model\Aqua_Router_Model_Route $route
	 * @return void
	 */
	public function addRoute(Aqua_Router_Model_Route $route) {
		if($route !== null) {
			$this->map[] = $route;
		}
	}
	
    /**
     * Parse a url into an array using the PHP parse_url function
     * @param string $url
     * @return string
     */
    public static function parseUrl($url) {
        $url = String::cast($url);
        //If the url contains a trailing slash, remove it for consistency
        if(substr($url, -1) === '/') {
            $url = substr($url, 0, -1);
        }
        
        return $url;
    }
    
    /**
     * Match a URL against a defined route
     * @param string $url
	 * @return \AquaCMS\Code\Core\Aqua\Router\Model\Aqua_Router_Model_Route
     */
    public function matchRoute($url) {
        /*
		 *  sort the map by key length descending (high to low string length)
		 *  We want this because of the URL format, ie:
		 *  the user visits "/post/edit/1" and we have 2 route's, 1 containing "/post/<something>" and 1 containing "/post/<action>/<something>"
		 *  we don't want the script to match against "/post/<something>" because the longer route is the correct match.
		 */
        krsort($this->map);
		
		$url = $this->parseUrl(String::cast($url));
		
		// Match against every route in the map, stop when a matching Route is found
        foreach($this->map as $route) {
			if(preg_match($this->convertRouteUrlToRegex($route->getUrl()), $url) === 1) {
				$result = clone $route;
				$result->setUrlSegments(Router::getRouteSegmentsFromUrl($result->getUrl(), $url));
				return $result;
			}
        }
		
		//Return null if no matching Route is found
		return null;
    }
			
	/**
	 * Converts a Route URL into a Regex Pattern for matching against
	 * @param string $url
	 * @return string
	 */
	public static function convertRouteUrlToRegex($url) {
		$parts = explode('/', String::cast($url));
		
		foreach($parts as &$value) {
			if(substr($value, 0, 1) === ":") {
				$value = "([a-z0-9]*)";
			} else {
				$value = "(" . $value . ")";
			}
		}
		
		return '/(' . implode('\/', $parts) . ')/';
	}
	
	/**
	 * Get the Dynamic Route Segments from the URL in their corresponding parameter names
	 * @param string $route
	 * @param string $url
	 * @return array
	 */
	public static function getRouteSegmentsFromUrl($routeUrl, $url) {
		$result		= array();
		$i			= 0;
		$RouteParts = explode('/', String::cast($routeUrl));
		$UrlParts	= explode('/', Router::parseUrl(String::cast($url)));
		
		foreach($RouteParts as $value) {
			if(substr($value, 0, 1) === ":") {
				$result[substr($value, 1)] = $UrlParts[$i];
			}
			
			$i++;
		}
		
		return $result;
	}
}