<?php

/**
 *  The aqua-cms is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The aqua-cms is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use Aqua_DataType_Model_String as String;

/**
 * Create a route using this class
 *
 * @TODO complete rewrite
 *
 * @author Jorin
 */
final class Aqua_Router_Model_Route {
    private $url;
	private $model;
	private $view;
    private $controller;
    private $action;
    private $urlsegments;
	
	/**
	 * @param string $url
	 * @param string $model
	 * @param string $view
	 * @param string $controller
	 * @param string $action
	 */
    public function __construct($url, $model, $view, $controller, $action = 'index') {
        $this->url			= String::cast($url);
		$this->model		= String::cast($model);
		$this->view			= String::cast($view);
        $this->controller	= String::cast($controller);
        $this->action		= String::cast($action);
		$this->urlsegments	= array();
    }
	
	/**
	 * Get the RouteURL to match against
	 * @return string
	 */
	public function getUrl() {
		return $this->url;
	}
	
	/**
	 * Get the model to use
	 * @return string
	 */
	public function getModel() {
		return $this->model;
	}
	
	/**
	 * Get the View to use
	 * @return string
	 */
	public function getView() {
		return $this->view;
	}
	
	/**
	 * Get the Controller to use
	 * @return string
	 */
	public function getController() {
		return $this->controller;
	}
	
	/**
	 * Get the action to execute
	 * @return string
	 */
	public function getAction() {
		return $this->action;
	}
	
	/**
	 * Get the URL segments that matched the route ID
	 * @return array
	 */
	public function getUrlSegments() {
		return $this->urlsegments;
	}
	
	/**
	 * Set the URL segments
	 * @param array $urlsegments
	 */
	public function setUrlSegments(array $urlsegments) {
		$this->urlsegments = $urlsegments;
	}
}
