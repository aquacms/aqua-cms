<?php
/**
 * Assert
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to kontakt@beberlei.de so I can send you a copy immediately.
 */

class Aqua_Assert_Model_Assertion {
    /**
     * Variable containing all notices that got generated
     */
    protected $_notices = null;
    
    /**
     * Variable containing all warnings that got generated
     * @var array 
     */
    protected $_warnings = null;
    
    public function __construct() {
        
    }
    
    public function getWarnings() {
        return $this->_warnings;
    }
    
    
}