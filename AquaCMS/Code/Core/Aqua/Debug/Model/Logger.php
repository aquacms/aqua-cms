<?php

use Aqua_Debug_Model_Severity as Severity;
use Aqua_DataType_Model_String as String;

class Aqua_Debug_Model_Logger {
    /**
     * The name of the log that will be written to
     * @var string 
     */
    protected $_logFileName;
    
    /**
     * The filepath of the log that will be written to
     * @var string 
     */
    protected $_logFilePath;
    
    /**
     * The template line to use for logging
     * the order of items in array['format'] is used to build the order of the
     * output message.
     * @var type 
     */
    protected $_logTemplate;
    
    /**
     * Constructor for the logger class
     * 
     * @TODO Check if log directory is writable (sufficient permissions)
     * 
     * @param string $logFilename optional custom filename to write logs to
     */
    public function __construct($logFileName = '', $logFilePath = LOG_DIR) {
        if(String::isDefault($logFileName) === true) {
            $this->_logFileName = "log_" . date("d-m-Y") . ".txt";
        } else {
            $this->_logFileName = String::cast($logFileName);
        }
        
        $this->_logFilePath = String::cast($logFilePath);
        
        $this->_logTemplate = array(
            'format' => array(
                [0] => 'datetime',
                [1] => 'severity',
                [2] => 'message',
                [3] => 'file',
                [4] => 'line'
            ),
            'date'      => '[d-m-Y][H:i:s]',
            'severity'  => ' [%s]',
            'message'   => ' %s',
            'file'      => ' in %s',
            'line'      => ' at line %d'
        );
    }
    
    public function getLogFilename() {
        return $this->_logFilename;
    }
    
    public function log($message, $file = '', int $line = 0, Severity $severity = Severity::DEBUG) {
        file_put_contents(
            //File to write to
            $this->_logFilePath . $this->_logFileName,
            //The content to write
            $this->formatError(
                String::cast($message),
                String::cast($file),
                $line,
                $severity
            ),
            //Method for writing
            FILE_APPEND | LOCK_EX
        );
    }
    
    /**
     * Loop through the logTemplate format and create the message based
     * on this template
     * @param type $message
     * @param type $file
     * @param type $line
     * @param type $severity
     * @return string
     */
    private function formatError($message, $file, $line, Severity $severity) {
        $result = '';
        
        foreach($this->_logTemplate['format'] as $item) {
            if($item == 'date') {
                $result .= date($this->_logTemplate['date']);
            } elseif(ctype_alpha($item) && isset($this->_logTemplate[$item]) && !empty($this->_logTemplate[$item])) {
                $result .= sprintf($this->_logTemplate[$item], ${$item});
            }
        }
        
        return $result;
    }
}