<?php

final class Aqua_Debug_Model_Severity {
       
    const __default = self::UNDEFINED;
        
    const UNDEFINED = 'UNDEFINED';
    const STRICT    = 'STRICT';
    const NOTICE    = 'NOTICE';
    const WARNING   = 'WARNING';
    const ERROR     = 'ERROR';
    const PARSE     = 'PARSE';
    const DEBUG     = 'DEBUG';
    
    const UNDEFINED_INT     = 1;
    const STRICT_INT        = 2;
    const NOTICE_INT        = 3;
    const WARNING_INT       = 4;
    const ERROR_INT         = 5;
    const PARSE_INT         = 6;
    const DEBUG_INT         = 7;
}