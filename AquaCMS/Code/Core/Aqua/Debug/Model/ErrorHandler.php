<?php

use Aqua_DataType_Model_Boolean as Boolean;

/**
 * The ErrorHandler will replace the default PHP error handler
 * this way we can hook events, log the messages we want (and how we want it)
 * and prevent unwanted people from seeing certain parts of error messages
 * (for example a filename (or worse, filepath) and/or linenumber)
 */
class Aqua_DataType_Model_ErrorHandler {
    /**
     * This variable will contain all errors, warnings, etc
     * (depending on the value of error_reporting())
     * @var array
     */
    protected $_errors = null;
    
    /**
     * Suppress output that may be generated
     * @var bool
     */
    protected $_silenceOutput = true;
    
    public function __construct() {
        
    }
    
    public function handler(int $errno, string $errstr, string $errfile, int $errline, array $errcontext) {
        
    }
    
    /**
     * Wether to silence output or not, errors will always be stored and
     * parsed within the instance, regardless of this value.
     * @param bool $silence
     */
    public function silenceOutput($silence) {
        $silence = Boolean::cast($silence);
        $this->_silenceOutput = $silence;
    }
}