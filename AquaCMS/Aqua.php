<?php

/**
 *  The aqua-cms is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The aqua-cms is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use Aqua_Core_Model_Classloader as Classloader;
use Aqua_Registry_Model_Registry as Registry;
use Aqua_Core_Model_FileSystem as FileSystem;
use Aqua_Core_Model_PackageManager as PackageManager;

//Create a shorter constant for the directory separator
define('DS', DIRECTORY_SEPARATOR);

/**
 * Application paths
 */
define('BASE_DIR',  realpath(dirname(__FILE__) . DS . '..') . DS);
define('APP_DIR',   BASE_DIR . 'AquaCMS' . DS);
define('LOG_DIR',   BASE_DIR . 'AquaCMS' . DS);

include APP_DIR . 'Code/Core/Aqua/Core/Model/Classloader.php';

//DataTypes
include ClassLoader::getFile('Aqua/Core/Model/DataType/IDataType.php');
include ClassLoader::getFile('Aqua/Core/Model/DataType/Array.php');
include ClassLoader::getFile('Aqua/Core/Model/DataType/Boolean.php');
include ClassLoader::getFile('Aqua/Core/Model/DataType/Float.php');
include ClassLoader::getFile('Aqua/Core/Model/DataType/Integer.php');
include ClassLoader::getFile('Aqua/Core/Model/DataType/Object.php');
include ClassLoader::getFile('Aqua/Core/Model/DataType/String.php');

//FileSystem
include ClassLoader::getFile('Aqua/Core/Model/FileSystem/IFileSystemObject.php');
include ClassLoader::getFile('Aqua/Core/Model/FileSystem/Directory.php');
include ClassLoader::getFile('Aqua/Core/Model/FileSystem/File.php');
include ClassLoader::getFile('Aqua/Core/Model/FileSystem.php');

//Temp until classloader is operational
include ClassLoader::getFile('Aqua/Core/Model/PackageManager.php');
include ClassLoader::getFile('Aqua/Registry/Model/Registry.php');

final class Aqua {
    /**
     * Application model
     *
     * @var Object
     */
    static protected $_app = null;

    /**
     * The application's package manager
     * @var null
     */
    static protected $_packageManager = null;

    /**
     * Application register
     *
     * @var Object
     */
    static protected $_appRegister = null;

    /**
     * Is developer mode flag
     *
     * @var bool
     */
    static protected $_developerMode = false;
    
    /**
     * Logging enabled?
     * 
     * @var bool
     */
    static protected $_logEnabled = false;

    /**
     * Private constructor that initializes the application
     * @param array $options
     */
    private function __construct(array $options = array()) {
        //Register the autoloader function
        self::registerAutoloader();

        //Create application instance of the Registry
        self::$_appRegister     = new Registry;
        self::$_packageManager  = new PackageManager;

        //Set application options
        self::$_developerMode   = $options['developerMode'];
        self::$_logEnabled      = $options['logEnabled'];
    }

    /**
     * Get application singleton instance
     *
     * @TODO implement events system
     *
     * @param array $options
     * @return Core_App_Class
     */
    public static function app(array $options = array()) {
        if (self::$_app == null) {
            self::$_app = new self($options); //@TODO Replace with core app class
        }
        
        return self::$_app;
    }
    
    /**
     * Start executing logic when this is called
     *
     * @TODO add logic.
     */
    public function run() {
    }

    /**
     * Register the autoload function
     */
    public static function registerAutoloader() {
        spl_autoload_register('Aqua_Core_Model_Classloader::autoload');
    }

    protected static function getRegistry() {
        if(is_null(self::$_appRegister) === true) {
            self::$_appRegister = new Registry();
        }

        return self::$_appRegister;
    }

    /**
     * Set a value from the registry by key
     * if $update is set to true it will update any existing key
     *
     * @param $key
     * @param $value
     * @param bool $update
     * @param bool $graceful
     * @return bool
     * @throws Exception
     */
    public static function registrySet($key, $value, $update = false, $graceful = false) {
        return self::getRegistry()->set($key, $value, $update, $graceful);
    }

    /**
     * Get a value from the registry by key
     *
     * @param $key
     * @param bool $graceful
     * @throws Exception
     */
    public static function registryGet($key, $graceful = false) {
        return self::getRegistry()->get($key, $graceful);
    }

    /**
     * Delete a key from the registry
     *
     * @param $key
     * @return boolean
     */
    public static function registryDelete($key) {
        return self::getRegistry()->delete($key);
    }
    
    /**
     * Retrieve model object
     *
     * @param   string $className
     * @param   array $options
     * @return  Object|null
     */
    public static function getModel($className, array $options = array()) {
        return Classloader::getModel($className, $options);
    }

    /**
     * Retrieve model object singleton
     *
     * @param   string $className
     * @param   array $options
     * @return  Object|null
     */
    public static function getSingleton($className, array $options = array()) {
        return Classloader::getSingleton($className, $options);
    }
}